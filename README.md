# Predicting Pass Receiver In Football Using Distance Based Features

This repository contain the jupyter notebooks and datasets used for producing the results 
presented in our paper for the **MLSA2018** prediction challenge. 

## Datasets

Two datasets are in the [**_datasets_**](datasets) directory: [_passes.csv_](datasets/passes.csv)
and [_passes_left_right.csv_](datasets/passes_left_right.csv).

- [_passes.csv_](datasets/passes.csv): This dataset is the original dataset provided for the challenge
    ([github](https://github.com/JanVanHaaren/mlsa18-pass-prediction)). 
- [_passes_left_right.csv_](datasets/passes_left_right.csv): This dataset was obtained from the previous one to represent all attacks
    from left to right (_ie_ the opponent goal is on the right of the pass sender). 
    This dataset format is optional for the features used in the paper but allow more 
    normalization for advanced features. 
    
## Notebooks

The [_utils.py_](notebooks/utils.py) and [_pass_pred.ipynb_](notebooks/pass_pred.ipynb) are readable
on the gitlab web interface.
The notebook contains already our execution results.

The main notebook is [_pass_pred.ipynb_](notebooks/pass_pred.ipynb)
contained by the [**_notebooks_**](notebooks) directory.
This notebook contained the different experiments on prediction presented in the _results_ section of our paper.