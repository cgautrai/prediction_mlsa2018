# This file contains functions that are used to make predictions.
# These functions were previously defined in notebooks.
# They are centralized here to avoid duplicated codes

import numpy as np
import pandas as pd
import os
import json
from sklearn.model_selection import KFold

from utils import get_same_team


def score_predict_pass(pair_clf, kfold, df, pair_df, only_good_pass=False,
                       features_cols=["dist", "opponent_dist", "same_team"], export_dir=""):
    """
    Main function to call to perform classification
    :param pair_clf:
    :param kfold:
    :param df:
    :param pair_df:
    :param only_good_pass:
    :param features_cols:
    :param export_dir:
    :return:
    """
    kf = KFold(n_splits=kfold, shuffle=True, random_state=2)
    cv_scores_max_good = []
    cv_scores_max_fail = []
    mrr_good = []
    mrr_fail = []
    mrr_all = []

    for train_index, test_index in kf.split(df):
        pass_data_df = pair_df[pair_df["game_id"].isin(pd.Series(train_index))]
        class_column = "y"

        X = pass_data_df[features_cols]
        y = pass_data_df[class_column].ravel()
        pair_clf.fit(X, y)

        X_test_all, y_test_all = generate_global_features_by_class(pair_clf, df, pair_df, test_index, features_cols,
                                                                   export_dir=export_dir)

        if not only_good_pass:
            score = compute_score(X_test_all[0], y_test_all[0])
            # print("top3 max fail pass accuracy:", score)
            cv_scores_max_fail.append(score)
        score = compute_score(X_test_all[1], y_test_all[1])
        # print("top3 max good pass accuracy:", score)
        cv_scores_max_good.append(score)
        if not only_good_pass:
            mrr = compute_mrr(X_test_all[0], y_test_all[0])
            # print("mrr max fail pass accuracy:", mrr)
            mrr_fail.append(mrr)
        mrr = compute_mrr(X_test_all[1], y_test_all[1])
        # print("mrr max good pass accuracy:", mrr)
        mrr_good.append(mrr)
        if not only_good_pass:
            mrr = compute_mrr(X_test_all[0] + X_test_all[1], y_test_all[0] + y_test_all[1])
            # print("mrr max all pass accuracy:", mrr)
            mrr_all.append(mrr)

    return mrr_good, mrr_fail, cv_scores_max_good, cv_scores_max_fail, mrr_all

def generate_global_features_by_class(pair_clf, df, pairwise_df, indices, features_cols, export_dir=""):
    """
    This function makes a prediction given a trained classifier and a set of indices
    :param pair_clf: The classifier to predict the propability of a pass
    :param df: The overall pass dataframe
    :param pairwise_df: The dataframe of pairwise pass features
    :param indices: The indices of examples in the df dataframe
    :param features_cols: The feature column names
    :param export_dir: If set to a string, export the prediction results to the specified folder, creating a result.json file
    :return:
    """
    X_all = [[], []]
    y_all = [[], []]

    for idx, row in df.iloc[indices].iterrows():
        # We get the pairwise data for the tested game
        features = pairwise_df[pairwise_df["game_id"] == idx]
        player_ids = features["player_id"].tolist()
        X = features[features_cols]

        # We get class probabilities

        probas = [p[1] for p in pair_clf.predict_proba(X)]
        x = np.array(list(probas) + [int(row["sender_id"] <= 14), int(row['time_start'])], dtype=float)
        good_pass = int(get_same_team(int(row["sender_id"]), int(row["receiver_id"])))
        X_all[good_pass].append(np.array(player_ids + list(x)))
        y_all[good_pass].append(int(row["receiver_id"]))

        # We export result if asked
        if export_dir:
            if not os.path.isdir(export_dir):
                os.makedirs(export_dir)
            with open(os.path.join(export_dir, "results.json"), "a") as f:
                pred = {}
                pred["game_id"] = int(idx)
                pred["positions"] = [[row["x_" + str(j)], row["y_" + str(j)]] for j in range(1, 29)]
                pred["sender_id"] = row["sender_id"]
                pred["receiver_id"] = row["receiver_id"]
                pred["predictions"] = [player_ids[i] for i in np.argsort(probas)[::-1]]
                pred["probas"] = np.sort(probas)[::-1].tolist()
                json.dump(pred, f)
                f.write("\n")

    return X_all, y_all


def compute_score(X, y):
    score = [0] * 4
    for i in range(len(X)):
        x = list(X[i])
        len_players = int((len(x) - 2) / 2)
        players_id = x[:len_players]
        probas = x[len_players:2 * len_players]
        ranks = np.argsort(probas)[::-1]
        # mod = False
        for j in range(3):
            if players_id[ranks[j]] == y[i]:
                score[j] += 1

    score[3] = len(X)
    return score

def compute_mrr(X,y):
    sum_mrr = 0
    for i in range(len(X)):
        x = list(X[i])
        len_players = int((len(x)-2) / 2)
        players_id = x[:len_players]
        probas = x[len_players:2*len_players]
        ranks = np.argsort(probas)[::-1]
        for j in range(len(ranks)):
            if players_id[ranks[j]] == y[i]:
                sum_mrr += 1/float(j+1)
    return sum_mrr / float(len(X))

def topk_accuracy(acc_vec, k):
    return [float(sum(acc[:k]))/float(acc[-1]) for acc in acc_vec]


def print_score_results(cv_score, only_good_pass=False):
    print("--")
    print("mrr good pass mean accuracy:", np.mean(cv_score[0]))
    print("mrr good pass std accuracy:", np.std(cv_score[0]))
    print("--")
    if not only_good_pass:
        print("mrr fail pass mean accuracy:", np.mean(cv_score[1]))
        print("mrr fail pass std accuracy:", np.std(cv_score[1]))
        print("--")
        print("mrr all pass mean accuracy:", np.mean(cv_score[4]))
        print("mrr all pass std accuracy:", np.std(cv_score[4]))
        print("--")

    top_good_pass = cv_score[2]
    if not only_good_pass:
        top_fail_pass = cv_score[3]
        top_pass = np.sum([top_good_pass, top_fail_pass], axis=0)

    for i in range(1, 4):
        print("top" + str(i) + " good pass mean accuracy:", np.mean(topk_accuracy(top_good_pass, i)))
        print("top" + str(i) + " good pass std accuracy:", np.std(topk_accuracy(top_good_pass, i)))
        print("--")

    if not only_good_pass:
        for i in range(1, 4):
            print("top" + str(i) + " fail pass mean accuracy:", np.mean(topk_accuracy(top_fail_pass, i)))
            print("top" + str(i) + " fail pass std accuracy:", np.std(topk_accuracy(top_fail_pass, i)))
            print("--")

        for i in range(1, 4):
            print("top" + str(i) + " pass mean accuracy:", np.mean(topk_accuracy(top_pass, i)))
            print("top" + str(i) + " pass std accuracy:", np.std(topk_accuracy(top_pass, i)))
            print("--")