import math
import numpy as np
import pandas as pd
from scipy.spatial import distance
from sklearn.model_selection import StratifiedKFold, KFold


def get_closest_opponent_dist(sender_pos, player_pos, opponents_pos):
    if player_pos[0] == sender_pos[0]:
        return min([abs(pos[0] - player_pos[0]) for pos in opponents_pos])
    else:
        a = -(sender_pos[1] - player_pos[1]) / (sender_pos[0] - player_pos[0])
        b = -(sender_pos[1] - a * sender_pos[0])

        return min([abs(a * pos[0] + pos[1] + b) / math.sqrt(a ** 2 + 1) for pos in opponents_pos])


def get_closest_defender_dist(player_pos, opponents_pos, n=1):
    return sorted([(player_pos[0] - p[0]) ** 2 + (player_pos[1] - p[1]) ** 2 for p in opponents_pos])[:n]


def get_grid_number(player_pos, nb_x_grids=9, nb_y_grids=6):
    """
    Returns the grid number of the player on the discretized field
    :param player_pos:
    :param nb_x_grids:
    :param nb_y_grids:
    :return:
    """
    norm_x = player_pos[0] + 5250
    norm_y = player_pos[1] + 3400

    grid_size_x = int(5250*2/nb_x_grids)
    grid_size_y = int(3400*2/nb_y_grids)

    return int(norm_x/grid_size_x)*nb_y_grids + int(norm_y/grid_size_y)


def get_features(sender_id, player_id, df_line):
    """
    Return the feature vector fro the player pair.
    This assumes both players both have non NaN coordinates
    :param sender_id: The pass sender _id
    :param player_id: The player id to study
    :param df_line: The dataframe line (context of the pass)
    :return: A dictionary with keys being the feature names, and values their values :)
    """
    sender_pos = [df_line["x_" + str(sender_id)], df_line["y_" + str(sender_id)]]
    player_pos = [df_line["x_" + str(player_id)], df_line["y_" + str(player_id)]]

    opponents_indices = list(range(15, 29)) if sender_id < 15 else list(range(1, 15))
    opponents_pos = [[df_line["x_" + str(p_id)], df_line["y_" + str(p_id)]] for p_id in opponents_indices if
                     not np.isnan(df_line["y_" + str(p_id)])]

    features = {}
    features["y"] = 1 if player_id == df_line["receiver_id"] else 0

    features["dist"] = distance.euclidean(sender_pos, player_pos)
    features["opponent_dist"] = get_closest_opponent_dist(sender_pos, player_pos, opponents_pos)

    closest_defenders_dist = get_closest_defender_dist(player_pos, opponents_pos, 2)
    for i, d in enumerate(closest_defenders_dist):
        features[str(i) + "_defender_dist"] = d

    closest_defenders_dist = get_closest_defender_dist(sender_pos, opponents_pos, 2)
    for i, d in enumerate(closest_defenders_dist):
        features[str(i) + "_sender_defender_dist"] = d

    features["forward_distance"] = player_pos[0] - sender_pos[0]
    features["sender_grid"] = get_grid_number(sender_pos)
    features["player_grid"] = get_grid_number(player_pos)

    return features


def get_same_team(sender_id, player_id):
    return (sender_id < 15 and player_id < 15) or (sender_id > 14 and player_id > 14)


def get_pairwise_features_df(row, idx, use_start_time=False, use_both_team=False, add_team=False):
    sender_id = int(row["sender_id"])
    friendly_indices = list(range(1, 15)) if sender_id < 15 else list(range(15, 29))
    if use_both_team:
        friendly_indices = list(range(1, 29))

    pairwise_features = []
    # player_ids = []
    for player_id in friendly_indices:
        if not np.isnan(row["x_" + str(sender_id)]) and not np.isnan(row["y_" + str(sender_id)]):
            if player_id != sender_id:
                if not np.isnan(row["x_" + str(player_id)]) and not np.isnan(row["y_" + str(player_id)]):
                    pair_features = get_features(sender_id, player_id, row)
                    pair_features["player_id"] = player_id
                    pair_features["game_id"] = idx
                    if use_start_time:
                        pair_features["time_start"] = row["time_start"]
                    if add_team:
                        pair_features["same_team"] = get_same_team(sender_id, player_id)
                    pairwise_features.append(pair_features)
                    # player_ids.append(player_id)
    # return pd.DataFrame(pairwise_features), player_ids
    return pairwise_features


def good_pass(x):
    return (x['sender_id'] < 15 and x['receiver_id'] < 15) or (x['sender_id'] > 14 and x['receiver_id'] > 14)

